﻿using System.Windows.Forms;

namespace Diagnostic.Shell
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Text = "";

            this.components.Add(mainMenu_);

            var chooseBlockLabel = new ToolStripLabel("Choose block:");
            var chooseErrorLabel = new ToolStripLabel("Choose const error:");
            this.MainMenuStrip = mainMenu_;
            mainMenu_.Dock = DockStyle.Top;
            mainMenu_.Items.AddRange(new ToolStripItem[] {
                //chooseBlockLabel,
                //schemaBlockChooser_,
                //chooseErrorLabel,
                //schemaErrorChooser_,
                solveButton});

            schemaErrorChooser_.DropDownStyle = ComboBoxStyle.DropDownList;
            schemaBlockChooser_.DropDownStyle = ComboBoxStyle.DropDownList;
            solveButton.Text = "Solve";

            
            tabControl_.Dock = DockStyle.Fill;

            this.Controls.Add(tabControl_);
            this.Controls.Add(mainMenu_);
        }

        private MenuStrip mainMenu_ = new MenuStrip();
        private ToolStripComboBox schemaBlockChooser_ = new ToolStripComboBox();
        private ToolStripComboBox schemaErrorChooser_ = new ToolStripComboBox();
        private ToolStripButton solveButton = new ToolStripButton();
        private TabControl tabControl_ = new TabControl();

        #endregion
    }
}

