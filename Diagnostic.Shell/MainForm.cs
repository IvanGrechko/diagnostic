﻿using Diagnostic.Model;
using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Diagnostic.Shell
{
    public partial class MainForm : Form
    {
        private readonly ISchema schema_;
        private readonly TabPage schemaPage_;
        public MainForm()
        {
            InitializeComponent();
            schema_ = new SchemaFactory().CreateSchema();
            schemaPage_ = CreateSchemaTabPage();
            this.Load += Loaded;
        }

        private void Loaded(object sender, EventArgs e)
        {
            var allBlocks = schema_.GetAllBlocks().Where(b => !(b is IInput)).ToList();
            schemaBlockChooser_.Items.AddRange(allBlocks.OrderBy(b => b.BlockId).ToArray());
            schemaErrorChooser_.Items.Add(LogicalValue.Set);
            schemaErrorChooser_.Items.Add(LogicalValue.NotSet);
            schemaErrorChooser_.SelectedIndexChanged += SchemaXChooser__SelectedIndexChanged;
            schemaBlockChooser_.SelectedIndexChanged += SchemaXChooser__SelectedIndexChanged;

            tabControl_.TabPages.Add(schemaPage_);

            solveButton.Click += SolveButton_Click;
            solveButton.Enabled = true;
        }

        private void SchemaXChooser__SelectedIndexChanged(object sender, EventArgs e)
        {
            //solveButton.Enabled = schemaBlockChooser_.SelectedIndex != -1 && 
            //    schemaErrorChooser_.SelectedIndex != -1;
        }

        private void SolveButton_Click(object sender, EventArgs e)
        {
            var solver1 = new Solver.Active1dPathSolver(schema_);

            var inputs = schema_.GetInputBlocks();

            var allTestCases = new List<TestCasesTableRow[]>();
            foreach (var element in schema_.GetAllBlocks().Where(b => !(b is IInput)))
            {
                var constError0 = solver1.Solve(element, LogicalValue.NotSet);
                var constError1 = solver1.Solve(element, LogicalValue.Set);

                allTestCases.Add(constError0.Rows.ToArray());
                allTestCases.Add(constError1.Rows.ToArray());
            }

            var initialStateSolver = new Solver.InitialStateLfsrSolver();
            var initialState = initialStateSolver.FindInitialState(allTestCases, inputs.Length);
            var inputBlocks = initialState.Item1.InputValues.Select((v, i) => (IBlock)new InputBlock($"L{i + 1}"));
            var initialStateTable = new TestCasesTable(inputBlocks, new[] { initialState.Item1 });

            tabControl_.TabPages.Clear();
            tabControl_.TabPages.Add(schemaPage_);
            tabControl_.TabPages.Add(CreateTestCasePage(initialStateTable, $"Lab3. Initial State. ({initialState.Item2})"));
        }

        private TabPage CreateTestCasePage(TestCasesTable table, string name)
        {
            var tabPage = new TabPage();
            tabPage.Text = name;

            var dataTable = new DataTable();
            var gridView = new DataGridView();
            gridView.ReadOnly = true;
            gridView.Dock = DockStyle.Fill;
            gridView.DataSource = dataTable;

            tabPage.Controls.Add(gridView);

            foreach (var variable in table.Variables)
            {
                dataTable.Columns.Add(variable.BlockId, typeof(LogicalValue));
            }
            foreach (var row in table.Rows)
            {
                dataTable.Rows.Add(row.InputValues.ToArray());
            }

            gridView.DataBindingComplete += GridView_DataBindingComplete;

            return tabPage;
        }

        private void GridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var gridView = sender as DataGridView;
            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                gridView.Rows[i].HeaderCell.Value = (i + 1).ToString();
            }
        }

        private TabPage CreateSchemaTabPage()
        {
            var imageBox = new PictureBox();
            var tabPage = new TabPage();
            tabPage.Controls.Add(imageBox);

            imageBox.Dock = DockStyle.Fill;
            imageBox.Image = Images.Schema;
            tabPage.Text = "Schema";

            return tabPage;
        }
    }
}
