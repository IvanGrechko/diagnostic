﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Diagnostic.Shell
{
    public partial class ProgressForm : Form
    {
        private readonly ProgressBar progressBar_;
        public ProgressForm()
        {
            InitializeComponent();

            progressBar_ = new ProgressBar();
            progressBar_.Style = ProgressBarStyle.Marquee;
            progressBar_.Dock = DockStyle.Fill;

            this.Controls.Add(progressBar_);

            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
        }
    }
}
