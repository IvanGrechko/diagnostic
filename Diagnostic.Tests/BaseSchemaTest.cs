﻿using Diagnostic.Model;
using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Tests
{
    public class BaseSchemaTest
    {
        protected static ISchema CreateSchema()
        {
            return new TestSchemaFactory().CreateSchema();
        }

        protected IEnumerable<LogicalValue> Calculate(TestCasesTable table, ISchema schema)
        {
            var inputs = schema.GetAllBlocks().OfType<IInputSubstitute>().ToDictionary(r => r.BlockId);
            
            foreach (var row in table.Rows)
            {
                for (int index = 0; index < row.InputValues.Count; index++)
                {
                    var input = table.Variables[index];
                    inputs[input.BlockId].SetValue(row.InputValues[index]);
                }

                var output = ((IBlockSubstitute)schema.GetOutputBlock()).Calculate();
                yield return output;
            }
        }

        public static IEnumerable<object[]> TestCases
        {
            get
            {
                var schema = CreateSchema();
                var blocks = schema.GetAllBlocks().Where(b => !(b is IInputSubstitute)).
                    Select(b => b.BlockId)
                    .ToList();

                foreach (var block in blocks)
                {
                    yield return new object[] { block, LogicalValue.Set };
                    yield return new object[] { block, LogicalValue.NotSet };
                }
            }
        }
    }
}
