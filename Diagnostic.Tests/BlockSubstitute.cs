﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostic.Model;
using Diagnostic.Schema;

namespace Diagnostic.Tests
{
    public class BlockSubstitute : IBlockSubstitute
    {
        private readonly IBlock block_;
        private LogicalValue currentValue_;

        public BlockSubstitute(IBlock block)
        {
            block_ = block;
        }

        public string BlockId => block_.BlockId;

        public LogicalValue Calculate()
        {
            if (currentValue_ != null) return currentValue_;

            var dependencies = GetDependencies();
            var dependentValues = dependencies.Cast<IBlockSubstitute>().Select(s => s.Calculate()).ToArray();
            return block_.Calculate(dependentValues);
        }

        public LogicalValue Calculate(LogicalValue[] input)
        {
            return block_.Calculate(input);
        }

        public void ClearValue()
        {
            currentValue_ = null;
        }

        public LogicalValue GetCurrentValue()
        {
            return currentValue_;
        }

        public IEnumerable<IBlock> GetDependencies()
        {
            return block_.GetDependencies();
        }

        public TruthTable GetTruthTable()
        {
            return block_.GetTruthTable();
        }

        public void SetValue(LogicalValue value)
        {
            currentValue_ = value;
        }
    }
}
