﻿using Diagnostic.Model;
using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Tests
{
    public interface IBlockSubstitute : IBlock
    {
        LogicalValue Calculate();

        void SetValue(LogicalValue value);

        LogicalValue GetCurrentValue();

        void ClearValue();
    }
}
