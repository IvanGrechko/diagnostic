﻿using Diagnostic.Model;
using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Tests
{
    public interface IInputSubstitute : IBlockSubstitute, IInput
    {

    }
    public class InputSubstitute : IInputSubstitute
    {
        private readonly IInput input_;
        private LogicalValue currentValue_;

        public InputSubstitute(IInput input)
        {
            input_ = input;
        }
        public string BlockId => input_.BlockId;

        public LogicalValue Calculate()
        {
            if (currentValue_ == null) throw new Exception("Value is not set for the input.");
            return currentValue_;
        }

        public LogicalValue Calculate(LogicalValue[] input)
        {
            return input_.Calculate(input);
        }

        public void ClearValue()
        {
            currentValue_ = null;
        }

        public LogicalValue GetCurrentValue()
        {
            return currentValue_;
        }

        public IEnumerable<IBlock> GetDependencies()
        {
            return input_.GetDependencies();
        }

        public TruthTable GetTruthTable()
        {
            return input_.GetTruthTable();
        }

        public void SetValue(LogicalValue value)
        {
            currentValue_ = value;
        }
    }
}
