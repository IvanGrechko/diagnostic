﻿using Diagnostic.Memory;
using Diagnostic.Memory.Error;
using Diagnostic.Memory.Test;
using Diagnostic.Model;
using System;
using System.Linq;
using Xunit;

namespace Diagnostic.Tests
{
    public class MemoryTests : MemoryTestsBase
    {
        [Fact]
        public void ReadWriteTest()
        {
            var memory = new MemoryGrid(150);
            var baseAddress = memory.GetBaseAddress();

            memory.Write(baseAddress, LogicalValue.Set);

            var nextAddress = baseAddress.AddOffset(75);
            memory.Write(nextAddress, LogicalValue.Set);

            var lastAddress = baseAddress.AddOffset(memory.GetMaxOffset());
            memory.Write(lastAddress, LogicalValue.Set);

            Assert.Equal(LogicalValue.Set, memory.Read(baseAddress));
            Assert.Equal(LogicalValue.Set, memory.Read(nextAddress));
            Assert.Equal(LogicalValue.Set, memory.Read(lastAddress));
        }

        [Fact]
        public void ReadWriteArrayTest()
        {
            var memory = new MemoryGrid(150);
            var baseAddress = memory.GetBaseAddress();

            var nextAddress = baseAddress.AddOffset(74);
            var values = Enumerable.Range(0, 75).Select(i => LogicalValue.Set).ToArray();
            memory.Write(nextAddress, values);

            var readValues = memory.Read(nextAddress, 75);

            Assert.NotNull(readValues);
            Assert.Equal(75, readValues.Length);
            Assert.Equal(values, readValues);
        }
    }
}
