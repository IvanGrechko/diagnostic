﻿using Diagnostic.Memory.Test;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Tests
{
    public class MemoryTestsBase
    {
        protected IMemoryTest CreateTest(TestAlgorithm algorithm)
        {
            switch (algorithm)
            {
                case TestAlgorithm.Walking01:
                    return new Walking01();
                case TestAlgorithm.AlgorithmB:
                    return new AlgorithmB();
                case TestAlgorithm.MarchPS:
                    return new MarchPS();
                default:
                    throw new NotSupportedException();
            }
        }

        public enum TestAlgorithm
        {
            Walking01,

            AlgorithmB,

            MarchPS
        }
    }
}
