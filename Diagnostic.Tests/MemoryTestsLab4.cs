﻿using Diagnostic.Memory;
using Diagnostic.Memory.Error;
using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Diagnostic.Tests
{
    public class MemoryTestsLab4 : MemoryTestsBase
    {
        [Theory]
        [InlineData(TestAlgorithm.Walking01)]
        [InlineData(TestAlgorithm.AlgorithmB)]
        [InlineData(TestAlgorithm.MarchPS)]
        public void SafErrorTest(TestAlgorithm algorithm)
        {
            var test = CreateTest(algorithm);
            var memory = new MemoryGrid(150);
            var errorMemory = new ErrorMemory(memory);

            var address1 = errorMemory.GetBaseAddress().AddOffset(18);
            var saf1 = new StackAtFaults(address1, LogicalValue.Set);

            var address2 = errorMemory.GetBaseAddress().AddOffset(56);
            var saf2 = new StackAtFaults(address2, LogicalValue.Set);

            var address3 = errorMemory.GetBaseAddress().AddOffset(78);
            var saf3 = new StackAtFaults(address3, LogicalValue.NotSet);

            var address4 = errorMemory.GetBaseAddress().AddOffset(77);
            var saf4 = new StackAtFaults(address4, LogicalValue.NotSet);

            errorMemory.SetError(saf1);
            errorMemory.SetError(saf2);
            errorMemory.SetError(saf3);
            errorMemory.SetError(saf4);
            var erros = test.FindErrors(errorMemory);

            Assert.NotNull(erros);
            Assert.Equal(4, erros.Length);
        }

        [Theory]
        [InlineData(TestAlgorithm.Walking01)]
        [InlineData(TestAlgorithm.AlgorithmB)]
        [InlineData(TestAlgorithm.MarchPS)]
        public void CFidErrorTest(TestAlgorithm algorithm)
        {
            var test = CreateTest(algorithm);
            var memory = new MemoryGrid(150);
            var errorMemory = new ErrorMemory(memory);

            var address1 = errorMemory.GetBaseAddress().AddOffset(18);
            var victim1 = address1.AddOffset(1);
            var cfi1 = new IdempotentCouplingFaults(address1, victim1, LogicalValue.Set, LogicalValue.Set);

            var address2 = victim1;
            var victim2 = address2.AddOffset(1);
            var cfi2 = new IdempotentCouplingFaults(address2, victim2, LogicalValue.Set, LogicalValue.NotSet);

            var victim3 = errorMemory.GetBaseAddress().AddOffset(89);
            var address3 = victim3.AddOffset(10);
            var cfi3 = new IdempotentCouplingFaults(address3, victim3, LogicalValue.Set, LogicalValue.Set);

            var victim4 = errorMemory.GetBaseAddress().AddOffset(14);
            var address4 = victim4.AddOffset(10);
            var cfi4 = new IdempotentCouplingFaults(address4, victim4, LogicalValue.Set, LogicalValue.NotSet);

            var address5 = errorMemory.GetBaseAddress().AddOffset(20);
            var victim5 = address5.AddOffset(3);
            var cfi5 = new IdempotentCouplingFaults(address5, victim5, LogicalValue.NotSet, LogicalValue.Set);

            var address6 = errorMemory.GetBaseAddress().AddOffset(25);
            var victim6 = address6.AddOffset(4);
            var cfi6 = new IdempotentCouplingFaults(address6, victim6, LogicalValue.NotSet, LogicalValue.NotSet);

            var victim7 = errorMemory.GetBaseAddress().AddOffset(30);
            var address7 = victim7.AddOffset(10);
            var cfi7 = new IdempotentCouplingFaults(address7, victim7, LogicalValue.NotSet, LogicalValue.Set);

            var victim8 = errorMemory.GetBaseAddress().AddOffset(135);
            var address8 = victim8.AddOffset(10);
            var cfi8 = new IdempotentCouplingFaults(address8, victim8, LogicalValue.NotSet, LogicalValue.NotSet);

            errorMemory.SetError(cfi1);
            errorMemory.SetError(cfi2);
            errorMemory.SetError(cfi3);
            errorMemory.SetError(cfi4);
            errorMemory.SetError(cfi5);
            errorMemory.SetError(cfi6);
            errorMemory.SetError(cfi7);
            errorMemory.SetError(cfi8);

            var errors = test.FindErrors(errorMemory);

            Assert.NotNull(errors);
            Assert.Equal(8, errors.Length);
        }

        [Theory]
        [InlineData(TestAlgorithm.Walking01)]
        [InlineData(TestAlgorithm.AlgorithmB)]
        [InlineData(TestAlgorithm.MarchPS)]
        public void CFinErrorTest(TestAlgorithm algorithm)
        {
            var test = CreateTest(algorithm);
            var memory = new MemoryGrid(150);
            var errorMemory = new ErrorMemory(memory);

            var address1 = errorMemory.GetBaseAddress().AddOffset(18);
            var victim1 = address1.AddOffset(1);
            var cfi1 = new InversionCouplingFaults(address1, victim1, LogicalValue.Set);

            var address2 = victim1;
            var victim2 = address2.AddOffset(1);
            var cfi2 = new InversionCouplingFaults(address2, victim2, LogicalValue.NotSet);

            var victim3 = errorMemory.GetBaseAddress().AddOffset(89);
            var address3 = victim3.AddOffset(1);
            var cfi3 = new InversionCouplingFaults(address3, victim3, LogicalValue.Set);

            var victim4 = errorMemory.GetBaseAddress().AddOffset(90);
            var address4 = victim3.AddOffset(1);
            var cfi4 = new InversionCouplingFaults(address4, victim4, LogicalValue.NotSet);

            errorMemory.SetError(cfi1);
            errorMemory.SetError(cfi2);
            errorMemory.SetError(cfi3);
            errorMemory.SetError(cfi4);

            var errors = test.FindErrors(errorMemory);

            Assert.NotNull(errors);
            Assert.Equal(4, errors.Length);
        }

        [Theory]
        [InlineData(TestAlgorithm.Walking01)]
        [InlineData(TestAlgorithm.AlgorithmB)]
        [InlineData(TestAlgorithm.MarchPS)]
        public void AfErrorTest(TestAlgorithm algorithm)
        {
            var test = CreateTest(algorithm);
            var memory = new MemoryGrid(150);
            var errorMemory = new ErrorMemory(memory);

            var address1 = errorMemory.GetBaseAddress().AddOffset(18);
            var victim1 = address1.AddOffset(1);
            var victim2 = address1.AddOffset(50);
            var victim3 = address1.AddOffset(70);
            var cfi1 = new AddressDecoderFaults(address1, memory, new[] { victim1, victim2, victim3 });

            errorMemory.SetError(cfi1);

            var errors = test.FindErrors(errorMemory);

            Assert.NotNull(errors);
            Assert.Equal(4, errors.Length);
        }
    }
}
