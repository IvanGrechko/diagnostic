﻿using Diagnostic.Memory;
using Diagnostic.Memory.Error;
using Diagnostic.Model;
using Xunit;

namespace Diagnostic.Tests
{
    public class MemoryTestsLab5 : MemoryTestsBase
    {
        [Theory]
        [InlineData(TestAlgorithm.Walking01)]
        [InlineData(TestAlgorithm.AlgorithmB)]
        [InlineData(TestAlgorithm.MarchPS)]
        public void ANPSFTest(TestAlgorithm algorithm)
        {
            var test = CreateTest(algorithm);
            var memory = new MemoryGrid(150);
            var errorMemory = new ErrorMemory(memory);

            var memoryBaseAddress = errorMemory.GetBaseAddress();

            var victim11 = memoryBaseAddress.AddOffset(25);
            var baseAddress1 = memoryBaseAddress.AddOffset(26);
            var victim12 = memoryBaseAddress.AddOffset(27);
            var anpsf1 = new ANPSF(baseAddress1, new[] { victim11, victim12 });

            var baseAddress2 = memoryBaseAddress.AddOffset(35);
            var victim21 = memoryBaseAddress.AddOffset(36);
            var victim22 = memoryBaseAddress.AddOffset(37);
            var anpsf2 = new ANPSF(baseAddress2, new[] { victim21, victim22 });

            var baseAddress3 = memoryBaseAddress.AddOffset(35);
            var victim31 = memoryBaseAddress.AddOffset(32);
            var victim32 = memoryBaseAddress.AddOffset(33);
            var anpsf3 = new ANPSF(baseAddress3, new[] { victim31, victim32 });

            errorMemory.SetError(anpsf1);
            errorMemory.SetError(anpsf2);
            errorMemory.SetError(anpsf3);

            var errors = test.FindErrors(errorMemory);

            Assert.NotNull(errors);
            Assert.Equal(3, errors.Length);
        }

        [Theory]
        [InlineData(TestAlgorithm.Walking01)]
        [InlineData(TestAlgorithm.AlgorithmB)]
        [InlineData(TestAlgorithm.MarchPS)]
        public void PNPSFTest(TestAlgorithm algorithm)
        {
            var test = CreateTest(algorithm);
            var memory = new MemoryGrid(150);
            var errorMemory = new ErrorMemory(memory);

            var address1 = errorMemory.GetBaseAddress();
            var victim1 = address1.AddOffset(25);
            var victim2 = address1.AddOffset(26);
            var baseAddress = address1.AddOffset(30);
            var victim6 = address1.AddOffset(32);
            var victim7 = address1.AddOffset(33);
            var victim8 = address1.AddOffset(34);
            var anpsf = new PNPSF(
                baseAddress,
                new[] { victim1, victim2,  victim6, victim7, victim8 },
                new[] { LogicalValue.Set,  LogicalValue.Set, LogicalValue.NotSet, LogicalValue.NotSet, LogicalValue.Set });

            errorMemory.SetError(anpsf);

            var errors = test.FindErrors(errorMemory);

            Assert.NotNull(errors);
            Assert.Single(errors);
        }
    }
}
