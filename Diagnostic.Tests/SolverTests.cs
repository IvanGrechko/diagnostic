﻿using Diagnostic.Model;
using Diagnostic.Schema;
using Diagnostic.Solver;
using System.Linq;
using Xunit;

namespace Diagnostic.Tests
{
    public class SolverTests : BaseSchemaTest
    {
        [Theory]
        [MemberData(nameof(TestCases), MemberType = typeof(BaseSchemaTest))]
        public void Solver1DTest(string blockId, LogicalValue errorValue)
        {
            var schema = CreateSchema();
            var solver = new Active1dPathSolver(schema);
            SolverTest(blockId, errorValue, solver, schema);
        }

        [Theory]
        [MemberData(nameof(TestCases), MemberType = typeof(BaseSchemaTest))]
        public void SolverMDTest(string blockId, LogicalValue errorValue)
        {
            var schema = CreateSchema();
            var solver = new ActiveMdPathSolver(schema);
            SolverTest(blockId, errorValue, solver, schema);
        }

        [Theory]
        [MemberData(nameof(TestCases), MemberType = typeof(BaseSchemaTest))]
        public void SolversCompareTest(string blockId, LogicalValue errorValue)
        {
            var schema = CreateSchema();
            var solver1 = new Active1dPathSolver(schema);
            var solver2 = new ActiveMdPathSolver(schema);

            var solution1 = solver1.Solve(schema.GetBlock(blockId), errorValue);
            var solution2 = solver2.Solve(schema.GetBlock(blockId), errorValue);

            Assert.Equal(solution1.Rows.Count, solution2.Rows.Count);
            Assert.Equal(solution1.Rows, solution2.Rows, TestCasesTableRow.DefaultEqualityComaprer);
        }

        private void SolverTest(string blockId, LogicalValue errorValue, ISolver solver, ISchema schema)
        {
            var errorBlock = schema.GetBlock(blockId);

            var cases = solver.Solve(errorBlock, errorValue);

            Assert.True(cases.Rows.Count > 0);

            var withoutError = Calculate(cases, schema).ToList();

            ((IBlockSubstitute)errorBlock).SetValue(errorValue);

            var withError = Calculate(cases, schema).ToList();

            Assert.Equal(withError.Count, withoutError.Count);
            for (int index = 0; index < withoutError.Count; index++)
            {
                Assert.NotEqual(withError[index], withoutError[index]);
            }
        }
    }
}
