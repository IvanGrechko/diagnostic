﻿using Diagnostic.Model;
using Diagnostic.Random;
using Diagnostic.Solver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xunit;

namespace Diagnostic.Tests
{
    public class TestCasesGeneratorTests : BaseSchemaTest
    {
        [Theory()]
        [MemberData(nameof(TestCases), MemberType = typeof(BaseSchemaTest))]
        public void GeneratorTests(string blockId, LogicalValue errorValue)
        {
            var schema = CreateSchema();
            var inputs = schema.GetInputBlocks();

            var testCases = new Active1dPathSolver(schema).Solve(schema.GetBlock(blockId), errorValue);

            //var initialStateSolver = new InitialStateLfsrSolver();
            //var state = initialStateSolver.FindInitialState(testCases.Rows, inputs.Length);
            //Assert.NotNull(state);
        }

        //[Fact]
        //public void Fact()
        //{
        //    var randomGenerator = new LInearfeedbackShiftRegister();
        //    var testCasesGenerator = new TestCasesGenerator(randomGenerator, 7);

        //    //foreach (var testCase in testCasesGenerator.GenerateTestCases())
        //    foreach(var value in randomGenerator.GetEnumeration())
        //    {
        //        //Debug.Print(value.ToString());
        //    }
        //}
    }
}
