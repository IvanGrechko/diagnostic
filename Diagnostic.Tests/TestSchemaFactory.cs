﻿using Diagnostic.Model;
using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Tests
{
    public class TestSchemaFactory : SchemaFactory
    {
        protected override IBlock ProcessBlock(IBlock block)
        {
            block = base.ProcessBlock(block);
            if (block is IInput input) return new InputSubstitute(input);
            return new BlockSubstitute(block);
        }
    }
}
