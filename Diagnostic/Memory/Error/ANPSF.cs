﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostic.Model;

namespace Diagnostic.Memory.Error
{
    /// <summary>
    /// Active Pattern Sensitivite Fault
    /// </summary>
    public class ANPSF : IMemoryError
    {
        private readonly IAddress baseAddress_;
        private readonly IAddress[] neighborAddresses_;

        public ANPSF(IAddress baseAddress, IAddress[] neighborAddresses)
        {
            baseAddress_ = baseAddress;
            neighborAddresses_ = neighborAddresses;
        }

        public void ApplyReadError(IMemoryGrid currentState)
        {
            //Do nothing here
        }

        public void ApplyWriteError(IMemoryGrid previousState, IMemoryGrid currentState)
        {
            var neighborsChangeCount = neighborAddresses_.Count(n => previousState.Read(n) != currentState.Read(n));
            if (neighborsChangeCount == 1)
            {
                var currentVictimValue = currentState.Read(baseAddress_);
                currentState.Write(baseAddress_, !currentVictimValue);
            }
        }
    }
}
