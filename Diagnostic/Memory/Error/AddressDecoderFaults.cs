﻿using System;
using System.Linq;
using Diagnostic.Model;

namespace Diagnostic.Memory.Error
{
    /// <summary>
    /// AF
    /// </summary>
    public class AddressDecoderFaults : IMemoryError
    {
        private readonly IAddress errorAddress_;
        private readonly IMemoryGrid memory_;
        private readonly IAddress[] referenceAddresses_;

        public AddressDecoderFaults(IAddress errorAddress, IMemoryGrid memory, IAddress[] referenceAddresses)
        {
            errorAddress_ = errorAddress;
            memory_ = memory;
            referenceAddresses_ = referenceAddresses.ToArray();
        }

        public LogicalValue ApplyOnRead(IAddress address, LogicalValue currentValue)
        {
            if (referenceAddresses_.Any(r => r.CompareTo(address) == 0))
            {
                currentValue = memory_.Read(errorAddress_);
            }

            return currentValue;
        }

        public LogicalValue ApplyOnWrite(IAddress address, LogicalValue currentValue)
        {
            if (referenceAddresses_.Any(r => r.CompareTo(address) == 0))
            {
                memory_.Write(errorAddress_, currentValue);
                return memory_.Read(address);
            }

            return currentValue;
        }

        public void ApplyReadError(IMemoryGrid currentState)
        {
            foreach (var refAddress in referenceAddresses_)
            {
                currentState.Write(errorAddress_, currentState.Read(refAddress));
            }
        }

        public void ApplyWriteError(IMemoryGrid previousState, IMemoryGrid currentState)
        {
            foreach (var refAddress in referenceAddresses_)
            {
                currentState.Write(refAddress, currentState.Read(errorAddress_));
            }
        }
    }
}
