﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Memory.Error
{
    public interface IMemoryError
    {
        void ApplyWriteError(IMemoryGrid previousState, IMemoryGrid currentState);

        void ApplyReadError(IMemoryGrid currentState);
    }
}
