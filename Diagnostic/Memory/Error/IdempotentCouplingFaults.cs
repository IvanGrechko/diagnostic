﻿using Diagnostic.Model;

namespace Diagnostic.Memory.Error
{
    /// <summary>
    /// CFID
    /// </summary>
    public class IdempotentCouplingFaults : IMemoryError
    {
        private readonly IAddress aggressive_;
        private readonly IAddress victim_;
        private readonly LogicalValue triggerValue_;
        private LogicalValue errorValue_;

        public IdempotentCouplingFaults(
            IAddress aggressive,
            IAddress victim,
            LogicalValue triggerValue,
            LogicalValue errorValue)
        {
            aggressive_ = aggressive;
            victim_ = victim;
            triggerValue_ = triggerValue;
            errorValue_ = errorValue;
        }

        public void ApplyReadError(IMemoryGrid currentState)
        {
            //Nothing do here
        }

        public void ApplyWriteError(IMemoryGrid previousState, IMemoryGrid currentState)
        {
            if (previousState.Read(aggressive_).IsSet != triggerValue_.IsSet &&
                currentState.Read(aggressive_).IsSet == triggerValue_.IsSet)
            {
                currentState.Write(victim_, errorValue_);
            }
        }
    }
}
