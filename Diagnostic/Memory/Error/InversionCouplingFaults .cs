﻿using System;
using Diagnostic.Model;

namespace Diagnostic.Memory.Error
{
    /// <summary>
    /// CFIN
    /// </summary>
    public class InversionCouplingFaults : IMemoryError
    {
        private readonly IAddress aggressive_;
        private readonly IAddress victim_;
        private readonly LogicalValue toValueTrigger_;

        public InversionCouplingFaults(
            IAddress aggressive, 
            IAddress victim,
            LogicalValue toValueTrigger)
        {
            aggressive_ = aggressive;
            victim_ = victim;
            toValueTrigger_ = toValueTrigger;
        }

        public void ApplyReadError(IMemoryGrid currentState)
        {
            //Nothing do here
        }

        public void ApplyWriteError(IMemoryGrid previousState, IMemoryGrid currentState)
        {
            if (previousState.Read(aggressive_).IsSet != toValueTrigger_.IsSet &&
                currentState.Read(aggressive_).IsSet == toValueTrigger_.IsSet)
            {
                var victimValue = currentState.Read(victim_);
                currentState.Write(victim_, !victimValue);
            }
        }
    }
}
