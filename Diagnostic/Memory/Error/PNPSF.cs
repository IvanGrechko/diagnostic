﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostic.Model;

namespace Diagnostic.Memory.Error
{
    /// <summary>
    /// Passive Pattern Sensitive Fault
    /// </summary>
    public class PNPSF : IMemoryError
    {
        private readonly IAddress baseAddress_;
        private readonly IAddress[] neigbors_;
        private readonly LogicalValue[] values_;

        public void ApplyReadError(IMemoryGrid currentState)
        {
            //Nothing do here
        }

        public void ApplyWriteError(IMemoryGrid previousState, IMemoryGrid currentState)
        {
            if (Enumerable.Range(0, values_.Length)
                .All(index => values_[index].IsSet == currentState.Read(neigbors_[index]).IsSet))
            {
                var baseValue = currentState.Read(baseAddress_);
                currentState.Write(baseAddress_, !baseValue);
            }
        }

        public PNPSF(IAddress baseAddress, IAddress[] neigbors, LogicalValue[] values)
        {
            baseAddress_ = baseAddress;
            neigbors_ = neigbors;
            values_ = values;

            if (neigbors_.Length != values.Length) throw new ArgumentOutOfRangeException();
        }
    }
}
