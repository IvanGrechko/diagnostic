﻿using System;
using System.Collections.Generic;
using System.Text;
using Diagnostic.Model;

namespace Diagnostic.Memory.Error
{
    /// <summary>
    /// SAF (Stack at faults)
    /// </summary>
    public class StackAtFaults : IMemoryError
    {
        private readonly IAddress address_;
        private readonly LogicalValue constErroValue_;

        public StackAtFaults(IAddress address, LogicalValue constErroValue)
        {
            address_ = address;
            constErroValue_ = constErroValue;
        }

        public void ApplyReadError(IMemoryGrid currentState)
        {
            //Do nothing here
        }

        public void ApplyWriteError(IMemoryGrid previousState, IMemoryGrid currentState)
        {
            currentState.Write(address_, constErroValue_);
        }
    }
}
