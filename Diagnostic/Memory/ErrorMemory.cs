﻿using System;
using System.Collections.Generic;
using System.Linq;
using Diagnostic.Memory.Error;
using Diagnostic.Model;

namespace Diagnostic.Memory
{
    public class ErrorMemory : IMemoryGrid
    {
        private readonly IMemoryGrid memory_;
        private readonly IList<IMemoryError> errors_;

        public ErrorMemory(IMemoryGrid memory)
        {
            memory_ = memory;
            errors_ = new List<IMemoryError>();
        }

        public void SetError(IMemoryError error)
        {
            errors_.Add(error);
        }

        public IAddress GetBaseAddress()
        {
            return memory_.GetBaseAddress();
        }

        public int GetMaxOffset()
        {
            return memory_.GetMaxOffset();
        }

        public LogicalValue Read(IAddress address)
        {
            var readMemory = ApplyReadErrors();
            return readMemory.Read(address);
        }

        public LogicalValue[] Read(IAddress startAddress, int count)
        {
            var readMemory = ApplyReadErrors();
            return readMemory.Read(startAddress, count);
        }

        public void Write(IAddress address, LogicalValue value)
        {
            var previousState = memory_.Clone();
            memory_.Write(address, value);
            ApplyWriteError(previousState);

        }

        public void Write(IAddress startAddress, LogicalValue[] values)
        {
            var previousState = memory_.Clone();
            memory_.Write(startAddress, values);
            ApplyWriteError(previousState);
        }

        private void ApplyWriteError(IMemoryGrid previousState)
        {
            foreach (var error in errors_)
            {
                error.ApplyWriteError(previousState, memory_);
            }
        }

        private IMemoryGrid ApplyReadErrors()
        {
            var memory = this.memory_.Clone();
            foreach (var error in errors_)
            {
                error.ApplyReadError(memory);
            }

            return memory;
        }

        public IMemoryGrid Clone()
        {
            var clone = new ErrorMemory(memory_.Clone());
            foreach (var error in errors_)
                clone.SetError(error);

            return clone;
        }
    }
}
