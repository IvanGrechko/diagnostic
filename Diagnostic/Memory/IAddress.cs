﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Memory
{
    public interface IAddress : IComparable<IAddress>
    {
        IAddress AddOffset(int offset);
    }
}
