﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Memory
{
    public interface IMemoryGrid
    {
        IAddress GetBaseAddress();

        int GetMaxOffset();

        void Write(IAddress address, LogicalValue value);

        void Write(IAddress startAddress, LogicalValue[] values);

        LogicalValue Read(IAddress address);

        LogicalValue[] Read(IAddress startAddress, int count);

        IMemoryGrid Clone();
    }

    public static class IMemoryGridExtensions
    {
        public static IAddress GetLastAddress(this IMemoryGrid memory)
        {
            return memory.GetBaseAddress().AddOffset(memory.GetMaxOffset());
        }
        public static IEnumerable<IAddress> GetAddressSpace(this IMemoryGrid memory)
        {
            var baseAddress = memory.GetBaseAddress();
            var maxOffset = memory.GetMaxOffset();
            for (int offset = 0; offset <= maxOffset; offset++)
            {
                yield return baseAddress.AddOffset(offset);
            }
        }
    }
}
