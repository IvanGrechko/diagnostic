﻿using System;
using System.Diagnostics.CodeAnalysis;
using Diagnostic.Model;

namespace Diagnostic.Memory
{
    public class MemoryGrid : IMemoryGrid
    {
        private Address baseAddress_;
        private int maxOffset_;
        private LogicalValue[] values_;

        public MemoryGrid(int size)
        {
            if (size <= 0) throw new ArgumentOutOfRangeException(nameof(size));
            maxOffset_ = size - 1;
            baseAddress_ = new Address(0, maxOffset_);
            values_ = new LogicalValue[size];
        }

        public IMemoryGrid Clone()
        {
            var clone = new MemoryGrid(values_.Length);
            Array.Copy(values_, 0, clone.values_, 0, values_.Length);

            return clone;
        }

        public IAddress GetBaseAddress()
        {
            return baseAddress_;
        }

        public int GetMaxOffset()
        {
            return maxOffset_;
        }

        public LogicalValue Read(IAddress address)
        {
            return values_[GetIndex(address)] ?? LogicalValue.NotSet;
        }

        public LogicalValue[] Read(IAddress startAddress, int count)
        {
            var startIndex = GetIndex(startAddress);
            if (startIndex + count - 1 > maxOffset_) throw new NotSupportedException("Addresses are out of range.");

            var values = new LogicalValue[count];
            Array.Copy(values_, startIndex, values, 0, count);
            return values;
        }

        public void Write(IAddress address, LogicalValue value)
        {
            values_[GetIndex(address)] = value ?? throw new ArgumentNullException();
        }

        public void Write(IAddress startAddress, LogicalValue[] values)
        {
            var startIndex = GetIndex(startAddress);
            if (startIndex + values.Length - 1 > maxOffset_) throw new NotSupportedException("Addresses are out of range.");

            Array.Copy(values, 0, values_, startIndex, values.Length);
        }

        private int GetIndex(IAddress address)
        {
            var localAddres = address as Address;
            if (localAddres == null) throw new NotSupportedException("This type of Addresses is not supported.");
            var index = localAddres.AsInt();
            if (index > maxOffset_) throw new NotSupportedException("Addresses is out of range.");

            return index;
        }

        private class Address : IAddress
        {
            private int offset_;
            private int maxOffset_;

            public Address(int index, int maxOffset)
            {
                offset_ = index;
                maxOffset_ = maxOffset;
            }

            public IAddress AddOffset(int offset)
            {
                var newOffset = offset_ + offset;
                if (newOffset > maxOffset_) throw new ArgumentOutOfRangeException(nameof(offset));

                return new Address(newOffset, maxOffset_);
            }

            public int AsInt()
            {
                return offset_;
            }

            public int CompareTo([AllowNull] IAddress other)
            {
                if (ReferenceEquals(this, other)) return 0;
                var otherAddress = (Address)other;
                return offset_.CompareTo(otherAddress.offset_);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;

                return obj is Address other && other.offset_ == offset_;
            }

            public override int GetHashCode()
            {
                return offset_.GetHashCode();
            }

            public override string ToString()
            {
                return offset_.ToString();
            }
        }
    }
}
