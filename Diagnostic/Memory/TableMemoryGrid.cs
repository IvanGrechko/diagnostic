﻿//using Diagnostic.Model;

//namespace Diagnostic.Memory
//{
//    public class TableMemoryGrid : IMemoryGrid
//    {
//        private readonly int colCount_;
//        private readonly int rowCount_;
//        private readonly IMemoryGrid memory_;

//        public TableMemoryGrid(int colCount, int rowCount)
//        {
//            memory_ = new MemoryGrid(colCount * rowCount);
//            colCount_ = colCount;
//            rowCount_ = rowCount;
//        }

//        public IAddress GetCellAddress(int column, int row)
//        {
//            var offset = row * colCount_ + column;
//            return GetBaseAddress().AddOffset(offset);
//        }

//        public IAddress GetBaseAddress()
//        {
//            return memory_.GetBaseAddress();
//        }

//        public int GetMaxOffset()
//        {
//            return memory_.GetMaxOffset();
//        }

//        public LogicalValue Read(IAddress address)
//        {
//            return memory_.Read(address);
//        }

//        public LogicalValue[] Read(IAddress startAddress, int count)
//        {
//            return memory_.Read(startAddress, count);
//        }

//        public LogicalValue[] ReadRow(int row)
//        {
//            var startOffset = row * colCount_;
//            var baseAddress = GetBaseAddress().AddOffset(startOffset);
//            return Read(baseAddress, colCount_);
//        }

//        public void Write(IAddress address, LogicalValue value)
//        {
//            Write(address, value);
//        }

//        public void Write(IAddress startAddress, LogicalValue[] values)
//        {
//            Write(startAddress, values);
//        }
//    }
//}
