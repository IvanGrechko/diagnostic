﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Memory.Test
{
    public class AlgorithmB : MarchBase
    {
        protected override void BuildTest(IMarchTestActionBuilder builder)
        {
            builder
                .AllAddresses()
                .WriteAll(LogicalValue.NotSet)
                .Forward()
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Forward()
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Backward()
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Backward()
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet);
        }
    }
}
