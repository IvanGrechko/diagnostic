﻿using Diagnostic.Model;
using System.Collections.Generic;
using System.Linq;

namespace Diagnostic.Memory.Test
{
    public class Galpat : IMemoryTest
    {
        public IAddress[] FindErrors(IMemoryGrid memory)
        {
            var errors = new HashSet<IAddress>();
            var addressSpace = memory.GetAddressSpace().ToList();

            var zeroValues = Enumerable.Repeat(LogicalValue.NotSet, addressSpace.Count).ToArray();
            var setValues = Enumerable.Repeat(LogicalValue.Set, addressSpace.Count).ToArray();

            memory.Write(memory.GetBaseAddress(), zeroValues);
            CheckForErrors(addressSpace, memory, LogicalValue.NotSet, errors);

            memory.Write(memory.GetBaseAddress(), setValues);
            CheckForErrors(addressSpace, memory, LogicalValue.Set, errors);

            return errors.ToArray();
        }

        private void CheckForErrors(
            IList<IAddress> addressSpace,
            IMemoryGrid memory, 
            LogicalValue expectedValue,
            ISet<IAddress> errors)
        {
            foreach (var baseAddress in addressSpace)
            {
                var sourceValue = memory.Read(baseAddress);
                var testValue = !sourceValue;
                memory.Write(baseAddress, testValue);
                foreach (var nextAddress in addressSpace)
                {
                    if (baseAddress.CompareTo(nextAddress) != 0)
                    {
                        if (memory.Read(baseAddress) != testValue)
                        {
                            errors.Add(baseAddress);
                        }

                        if (memory.Read(nextAddress) != expectedValue)
                        {
                            errors.Add(nextAddress);
                        }
                    }
                }

                memory.Write(baseAddress, sourceValue);
            }
        }
    }
}
