﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Memory.Test
{
    public interface IMarchTestExecutor
    {
        IAddress[] FindErrors(IMemoryGrid memory);
    }
}
