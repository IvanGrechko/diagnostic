﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Memory.Test
{
    public interface IMemoryTest
    {
        IAddress[] FindErrors(IMemoryGrid memory);
    }
}
