﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Memory.Test
{
    public abstract class MarchBase : IMemoryTest
    {
        public IAddress[] FindErrors(IMemoryGrid memory)
        {
            var builder = new MarchTestBuilder();
            BuildTest(builder);

            return builder.Build().FindErrors(memory);
        }

        protected abstract void BuildTest(IMarchTestActionBuilder builder);
    }
}
