﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Memory.Test
{
    public class MarchPS : MarchBase
    {
        protected override void BuildTest(IMarchTestActionBuilder builder)
        {
            builder
                .AllAddresses()
                .WriteAll(LogicalValue.NotSet)
                .Forward()
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Forward()
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Read(LogicalValue.Set)
                .Forward()
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Forward()
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Read(LogicalValue.NotSet);
        }
    }
}
