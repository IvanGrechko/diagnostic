﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Memory.Test
{
    public interface IMarchTestActionBuilder
    {
        IMarchAllAddressesActionBuilder AllAddresses();

        IMarchAddressTestActionBuilder Forward();

        IMarchAddressTestActionBuilder Backward();

    }

    public interface IMarchAllAddressesActionBuilder : IMarchTestActionBuilder
    {
        IMarchAllAddressesActionBuilder WriteAll(LogicalValue value);

        IMarchAllAddressesActionBuilder ReadAll(LogicalValue value);
    }

    public interface IMarchAddressTestActionBuilder : IMarchTestActionBuilder
    {
        IMarchAddressTestActionBuilder Read(LogicalValue value);

        IMarchAddressTestActionBuilder Write(LogicalValue value);
    }

    public class MarchTestBuilder : IMarchTestActionBuilder
    {
        private delegate void TestAction(ISet<IAddress> errors, IMemoryGrid memory);
        private List<TestAction> actions_ = new List<TestAction>();

        public IMarchAllAddressesActionBuilder AllAddresses()
        {
            return new AllAddressBuilder(this, actions_);
        }

        public IMarchAddressTestActionBuilder Backward()
        {
            return new EachAddressActionBuilder(this, actions_, false);
        }

        public IMarchAddressTestActionBuilder Forward()
        {
            return new EachAddressActionBuilder(this, actions_, true);
        }

        public IMarchTestExecutor Build()
        {
            return new ActionExecutor(actions_);
        }

        private class AllAddressBuilder : IMarchAllAddressesActionBuilder
        {
            private readonly MarchTestBuilder builder_;
            private readonly IList<TestAction> actions_;

            public AllAddressBuilder(MarchTestBuilder builder, IList<TestAction> actions)
            {
                builder_ = builder;
                actions_ = actions;
            }

            public IMarchAllAddressesActionBuilder AllAddresses()
            {
                return builder_.AllAddresses();
            }

            public IMarchAddressTestActionBuilder Backward()
            {
                return builder_.Backward();
            }

            public IMarchAddressTestActionBuilder Forward()
            {
                return builder_.Forward();
            }

            public IMarchAllAddressesActionBuilder ReadAll(LogicalValue value)
            {
                TestAction action = (errors, memory) =>
                {
                    var values = memory.Read(memory.GetBaseAddress(), memory.GetMaxOffset() + 1);
                    for (int index = 0; index < values.Length; index++)
                    {
                        if (values[index].IsSet != value.IsSet)
                        {
                            errors.Add(memory.GetBaseAddress().AddOffset(index));
                        }
                    }
                };

                actions_.Add(action);

                return this;
            }

            public IMarchAllAddressesActionBuilder WriteAll(LogicalValue value)
            {
                TestAction action = (errors, memory) =>
                {
                    var values = Enumerable.Repeat(value, memory.GetMaxOffset() + 1).ToArray();
                    memory.Write(memory.GetBaseAddress(), values);
                };

                actions_.Add(action);

                return this;
            }
        }

        private class EachAddressActionBuilder : IMarchAddressTestActionBuilder
        {
            private delegate void TestAddressAction(ISet<IAddress> errors, IMemoryGrid memory, IAddress address);
            private readonly MarchTestBuilder builder_;
            private readonly IList<TestAddressAction> actions_ = new List<TestAddressAction>();

            public EachAddressActionBuilder(
                MarchTestBuilder builder,
                IList<TestAction> actions,
                bool isForward)
            {
                builder_ = builder;
                TestAction commonAction = (error, memory) =>
                {
                    var addresses = memory.GetAddressSpace();
                    if (!isForward) addresses = addresses.Reverse();

                    foreach (var address in addresses)
                    {
                        foreach (var action in actions_)
                        {
                            action(error, memory, address);
                        }
                    }
                };
                actions.Add(commonAction);
            }

            public IMarchAllAddressesActionBuilder AllAddresses()
            {
                return builder_.AllAddresses();
            }

            public IMarchAddressTestActionBuilder Backward()
            {
                return builder_.Backward();
            }

            public IMarchAddressTestActionBuilder Forward()
            {
                return builder_.Forward();
            }

            public IMarchAddressTestActionBuilder Read(LogicalValue value)
            {
                TestAddressAction action = (error, memory, address) =>
                {
                    if (memory.Read(address).IsSet != value.IsSet)
                    {
                        error.Add(address);
                    }
                };
                actions_.Add(action);
                return this;
            }

            public IMarchAddressTestActionBuilder Write(LogicalValue value)
            {
                TestAddressAction action = (error, memory, address) =>
                {
                    memory.Write(address, value);
                };
                actions_.Add(action);

                return this;
            }
        }

        private class ActionExecutor : IMarchTestExecutor
        {
            private readonly IList<TestAction> actions_;

            public ActionExecutor(IList<TestAction> actions)
            {
                actions_ = actions.ToList();
            }
            public IAddress[] FindErrors(IMemoryGrid memory)
            {
                var errors = new HashSet<IAddress>();
                foreach (var action in actions_)
                {
                    action(errors, memory);
                }

                return errors.ToArray();
            }
        }
    }
}
