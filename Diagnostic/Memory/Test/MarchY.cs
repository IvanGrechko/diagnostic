﻿using Diagnostic.Model;

namespace Diagnostic.Memory.Test
{
    public class MarchY : MarchBase
    {
        protected override void BuildTest(IMarchTestActionBuilder builder)
        {
            builder
                .AllAddresses()
                .WriteAll(LogicalValue.NotSet)
                .Forward()
                .Read(LogicalValue.NotSet)
                .Write(LogicalValue.Set)
                .Read(LogicalValue.Set)
                .Backward()
                .Read(LogicalValue.Set)
                .Write(LogicalValue.NotSet)
                .Read(LogicalValue.NotSet)
                .AllAddresses()
                .ReadAll(LogicalValue.NotSet);
        }
    }
}
