﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Model
{
    public static class CollectionExtensions
    {
        public static int FindIndex<T>(this IReadOnlyList<T> list, Func<T, bool> matcher)
        {
            for (int index = 0; index < list.Count; index++)
            {
                if (matcher(list[index])) return index;
            }

            return -1;
        }
    }
}
