﻿using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Diagnostic.Model
{
    public class CubeTable : Table<CubeTableRow>
    {
        public CubeTable(IEnumerable<IBlock> variables, IEnumerable<CubeTableRow> rows) : base(variables, rows)
        {
            if (Rows.Any(r => r.InputValues.Count != Variables.Count))
            {
                throw new NotSupportedException();
            }
        }
    }

    public class CubeTableRow : TableRow<CubeValue>
    {
        public CubeTableRow(IEnumerable<CubeValue> values) : base(values)
        {
        }

        public CubeTableRow(IEnumerable<CubeValue> values, CubeValue result)
            : this(values.Append(result))
        {

        }

        public CubeValue Result => InputValues[InputValues.Count - 1];
    }
}
