﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Model
{
    public class CubeValue
    {
        private readonly CubeValueType valueType_;

        public static CubeValue Set { get; } = new CubeValue(CubeValueType.Set);

        public static CubeValue NotSet { get; } = new CubeValue(CubeValueType.NotSet);

        public static CubeValue D { get; } = new CubeValue(CubeValueType.D);

        public static CubeValue NotD { get; } = new CubeValue(CubeValueType.NotD);

        public static CubeValue X { get; } = new CubeValue(CubeValueType.X);

        public static implicit operator CubeValue(LogicalValue value)
        {
            return value.IsSet ? Set : NotSet;
        }

        public static bool operator ==(CubeValue left, CubeValue right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CubeValue left, CubeValue right)
        {
            return !Equals(left, right);
        }

        public static CubeValue Intersect(CubeValue left, CubeValue right)
        {
            if (left.valueType_ == right.valueType_) return new CubeValue(left.valueType_);
            if (left.valueType_ == CubeValueType.X) return new CubeValue(right.valueType_);
            if (right.valueType_ == CubeValueType.X) return new CubeValue(left.valueType_);
            if (left.valueType_ == CubeValueType.Set && right.valueType_ == CubeValueType.NotSet)
                return D;
            if (left.valueType_ == CubeValueType.NotSet && right.valueType_ == CubeValueType.Set)
                return NotD;

            throw new ArgumentException($"Operation can not be applied to {left} and {right}.");
        }

        private CubeValue(CubeValueType valueType)
        {
            valueType_ = valueType;
        }

       

        public override string ToString()
        {
            switch (valueType_)
            {
                case CubeValueType.Set: return "1";
                case CubeValueType.NotSet: return "0";
                case CubeValueType.D: return "d";
                case CubeValueType.NotD: return Encoding.UTF8.GetString(new byte[] { 0xC4, 0x91 });
                case CubeValueType.X: return "x";
                default: return valueType_.ToString();
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (ReferenceEquals(obj, null))
            {
                return false;
            }

            return obj is CubeValue otherCube && otherCube.valueType_ == valueType_;
        }

        public override int GetHashCode()
        {
            return valueType_.GetHashCode();
        }

        private enum CubeValueType
        {
            Set,

            NotSet,

            D,

            NotD,

            X
        }
    }
}
