﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Model
{
    public class LogicalValue
    {
        private readonly int value_;

        public static LogicalValue Set { get; } = new LogicalValue(1);
        public static LogicalValue NotSet { get; } = new LogicalValue(0);

        private LogicalValue(int value)
        {
            value_ = value;
        }

        public override string ToString()
        {
            return value_.ToString();
        }

        public bool IsSet => value_ == 1;

        /// <summary>
        /// OR
        /// </summary>
        public static LogicalValue operator | (LogicalValue left, LogicalValue right)
        {
            return new LogicalValue(left.value_ | right.value_);
        }

        /// <summary>
        /// AND
        /// </summary>
        public static LogicalValue operator &(LogicalValue left, LogicalValue right)
        {
            return new LogicalValue(left.value_ & right.value_);
        }

        /// <summary>
        /// XOR
        /// </summary>
        public static LogicalValue operator ^(LogicalValue left, LogicalValue right)
        {
            return new LogicalValue(left.value_ ^ right.value_);
        }

        /// <summary>
        /// Not
        /// </summary>
        public static LogicalValue operator !(LogicalValue left)
        {
            return left.IsSet ? NotSet : Set;
        }

        /// <summary>
        /// Equal
        /// </summary>
        public static bool operator ==(LogicalValue left, LogicalValue right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Not Equal
        /// </summary>
        public static bool operator !=(LogicalValue left, LogicalValue right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (ReferenceEquals(obj, null))
            {
                return false;
            }

            return obj is LogicalValue otherValue && value_ == otherValue.value_;
        }

        public override int GetHashCode()
        {
            return value_.GetHashCode();
        }
    }
}
