﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Model
{
    public static class Operations
    {
        public delegate LogicalValue BinaryOperation(params LogicalValue[] values);

        public delegate LogicalValue UnaryOperation(LogicalValue x);

        public readonly static BinaryOperation Or = values => values.Aggregate((l, r) => l | r);

        public readonly static BinaryOperation And = values => values.Aggregate((l, r) => l & r);

        public readonly static BinaryOperation Xor = values => values.Aggregate((l, r) => l ^ r);

        public readonly static UnaryOperation Not = (l) => !l;

        public readonly static BinaryOperation OrNot = values => Not(Or(values));

        public readonly static BinaryOperation AndNot = values => Not(And(values));
    }
}
