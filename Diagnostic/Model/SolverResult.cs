﻿using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Model
{
    public class SolverResult
    {
        private Dictionary<string, List<LogicalValue>> values_ = new Dictionary<string, List<LogicalValue>>();

        public void Add(string blockId, LogicalValue value)
        {
            if (!values_.TryGetValue(blockId, out var holder))
            {
                holder = new List<LogicalValue>();
                values_[blockId] = holder;
            }
            holder.Add(value);
        }

        public void Merge(SolverResult solverResult)
        {
            var currentValues = values_.ToList();
            var newValues = solverResult.values_.ToList();
            var currentCount = currentValues.Count > 0 ? currentValues[0].Value.Count : 0;
            var newCount = newValues.Count > 0 ? newValues[0].Value.Count : 0;
            if (newCount == 0) return;
            if (currentCount == 0)
            {
                foreach (var newValue in newValues)
                {
                    values_[newValue.Key] = newValue.Value;
                }
            }
            else
            {

                var targetCount = currentCount * newCount;
                values_.Clear();
                var commonKeys = currentValues.Select(v => v.Key).Union(newValues.Select(v => v.Key)).ToArray();
                foreach (var key in commonKeys)
                {
                    values_.Add(key, new List<LogicalValue>());
                }

                for (int currentIndex = 0; currentIndex < currentCount; currentIndex++)
                    for (int newIndex = 0; newIndex < newCount; newIndex++)
                    {
                        foreach (var currentValue in currentValues)
                        {
                            values_[currentValue.Key].Add(currentValue.Value[currentIndex]);
                        }

                        foreach (var newValue in newValues)
                        {
                            values_[newValue.Key].Add(newValue.Value[newIndex]);
                        }
                    }
            }
        }

        public TestCasesTable GetTestCasesTable(ISchema solver)
        {
            var keys = values_.Keys.OrderBy(_ => _).ToArray();
            var variables = keys.Select(k => solver.GetBlock(k)).ToList();

            var rowsCount = values_.Values.First().Count;
            var rows = new List<TestCasesTableRow>();
            for (int index = 0; index < rowsCount; index++)
            {
                var values = keys.Select(k => values_[k][index]);
                rows.Add(new TestCasesTableRow(values));
            }

            return new TestCasesTable(variables, rows);
        }

        public void MergeSameVariables(SolverResult solverResult)
        {
            if (values_.Count != 0 && values_.Keys.Intersect(solverResult.values_.Keys).Count() != values_.Count)
            {
                throw new ArgumentException("Can not merge same variable rows");
            }
            foreach (var newValue in solverResult.values_)
            {
                if (values_.TryGetValue(newValue.Key, out var holder))
                {
                    holder.AddRange(newValue.Value);
                }
                else
                {
                    values_.Add(newValue.Key, newValue.Value);
                }
            }
        }
    }
}
