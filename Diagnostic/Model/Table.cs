﻿using Diagnostic.Schema;
using System.Collections.Generic;
using System.Linq;

namespace Diagnostic.Model
{
    public class Table<TRow>
    {
        public IReadOnlyList<IBlock> Variables { get; }

        public IReadOnlyCollection<TRow> Rows { get; }

        public Table(IEnumerable<IBlock> variables, IEnumerable<TRow> rows)
        {
            Variables = variables.ToList().AsReadOnly();
            Rows = rows.ToList().AsReadOnly();
        }
    }
}
