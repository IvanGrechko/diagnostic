﻿using System.Collections.Generic;
using System.Linq;

namespace Diagnostic.Model
{
    public class TableRow<T>
    {
        public IReadOnlyList<T> InputValues { get; }

        public TableRow(IEnumerable<T> values)
        {
            InputValues = values.ToList().AsReadOnly();
        }

        public override string ToString()
        {
            var inputs = string.Join("|", InputValues.Select(v => v.ToString()));
            return inputs;
        }
    }
}
