﻿using Diagnostic.Schema;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Diagnostic.Model
{
    public class TestCasesTable : Table<TestCasesTableRow>
    {
        public TestCasesTable(IEnumerable<IBlock> variables, IEnumerable<TestCasesTableRow> rows)
            : base(variables, rows.Distinct(TestCasesTableRow.DefaultEqualityComaprer)
                  .OrderBy(_ => _, TestCasesTableRow.DefaultComaprer))
        {
        }
    }

    public class TestCasesTableRow : TableRow<LogicalValue>
    {
        private static readonly DefaultEqualityComparer comparer_ = new DefaultEqualityComparer();
        public static IEqualityComparer<TestCasesTableRow> DefaultEqualityComaprer => comparer_;
        public static IComparer<TestCasesTableRow> DefaultComaprer => comparer_;
        public TestCasesTableRow(IEnumerable<LogicalValue> values) : base(values)
        {
        }

        private class DefaultEqualityComparer : IEqualityComparer<TestCasesTableRow>, IComparer<TestCasesTableRow>
        {
            public int Compare([AllowNull] TestCasesTableRow x, [AllowNull] TestCasesTableRow y)
            {
                if (ReferenceEquals(x, y)) return 0;
                if (ReferenceEquals(x, null)) return -1;
                if (ReferenceEquals(null, y)) return 1;
                if (x.InputValues.Count > y.InputValues.Count) return 1;
                if (x.InputValues.Count < y.InputValues.Count) return -1;

                for (int index = 0; index < x.InputValues.Count; index++)
                {
                    var xValue = x.InputValues[index];
                    var yValue = y.InputValues[index];
                    if (xValue.IsSet != yValue.IsSet)
                    {
                        return xValue.IsSet ? 1 : -1;
                    }
                }
                return 0;
            }

            public bool Equals([AllowNull] TestCasesTableRow x, [AllowNull] TestCasesTableRow y)
            {
                return Compare(x, y) == 0;
            }

            public int GetHashCode([DisallowNull] TestCasesTableRow obj)
            {
                return obj.InputValues.Aggregate(0, (current, nextValue) => current ^ 397 ^ nextValue.GetHashCode());
            }
        }
    }
}
