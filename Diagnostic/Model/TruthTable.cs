﻿using Diagnostic.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Model
{

    public class TruthTable : Table<TruthTableRow>
    {
        public TruthTable(IEnumerable<IBlock> variables, IEnumerable<TruthTableRow> rows) : base(variables, rows)
        {
        }
    }

    public class TruthTableRow : TableRow<LogicalValue>
    {
        public LogicalValue Result { get; }
        public TruthTableRow(IEnumerable<LogicalValue> values, LogicalValue result) : base(values)
        {
            Result = result;
        }

        public override string ToString()
        {
            return $"{base.ToString()} = {Result}";
        }
    }
}
