﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Diagnostic.Random
{
    public class LInearfeedbackShiftRegister
    {
        
        private static readonly LogicalValue[] polynom_ = new LogicalValue[8]
        {
            LogicalValue.Set,
            LogicalValue.NotSet,
            LogicalValue.NotSet,
            LogicalValue.NotSet,
            LogicalValue.NotSet,
            LogicalValue.Set,
            LogicalValue.Set,
            LogicalValue.Set,
        };
        private byte state_;
        private int ticks_ = 0;

        public LInearfeedbackShiftRegister()
            :this (1)
        {
        }

        public LInearfeedbackShiftRegister(byte state)
        {
            if (state <= 0) throw new ArgumentOutOfRangeException(nameof(state));
            state_ = state;
        }

        public LogicalValue CurrentValue => ToLogicalValue(state_ & 1);

        public IEnumerable<LogicalValue> CurrentState
        {
            get
            {
                for (int index = 0; index < polynom_.Length; index++)
                {
                    yield return ToLogicalValue((state_ >> index) & 1);
                }
            }
        }

        public IEnumerable<LogicalValue> GetEnumeration()
        {
            while (true)
            {
                MoveNext();
                yield return CurrentValue;
            }
        }

        public void MoveNext()
        {
            if (ticks_ > 0)
            {
                byte result = 0;
                bool isInitited = false;
                for (int index = 0; index < polynom_.Length; index++)
                {
                    if (polynom_[index].IsSet)
                    {
                        var output = (state_ >> index);
                        result = isInitited ? (byte)(output ^ result) : (byte)output;
                        isInitited = true;
                    }
                }
                state_ = (byte)((result & 1) << 7 | (state_ >> 1));
            }
            ticks_++;
        }

        public void MoveNext(int steps)
        {
            if (steps < 0) throw new ArgumentException("Steps count can not be less the 0", nameof(steps));
            for (int iteration = 0; iteration < steps; iteration++)
                MoveNext();
        }

        private LogicalValue ToLogicalValue(int value)
        {
            if (value == 1) return LogicalValue.Set;
            if (value == 0) return LogicalValue.NotSet;

            throw new NotSupportedException();
        }
    }
}
