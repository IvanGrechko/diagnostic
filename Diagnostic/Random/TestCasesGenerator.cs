﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Random
{
    public class TestCasesGenerator
    {
        private readonly LInearfeedbackShiftRegister randomGenerator_;
        private readonly int demension_;
        private readonly Queue<LogicalValue> testCasesQueue_;

        public TestCasesGenerator(LInearfeedbackShiftRegister randomGenerator, int demension)
        {
            if (demension <= 0) throw new ArgumentOutOfRangeException(nameof(demension));
            randomGenerator_ = randomGenerator;
            demension_ = demension;
            testCasesQueue_ = new Queue<LogicalValue>();
        }

        public IEnumerable<TestCasesTableRow> GenerateTestCases()
        {
            while(true)
            {
                var testCase = GenerateNextTestCase();
                yield return testCase;
                //yield return new TestCasesTableRow(testCase.InputValues.Reverse());
            }
        }

        private TestCasesTableRow GenerateNextTestCase()
        {
            if (testCasesQueue_.Count == 0)
            {
                foreach(var randomValue in randomGenerator_.GetEnumeration().Take(demension_))
                {
                    testCasesQueue_.Enqueue(randomValue);
                }
            }
            else
            {
                testCasesQueue_.Dequeue();
                testCasesQueue_.Enqueue(randomGenerator_.GetEnumeration().First());
            }
            return new TestCasesTableRow(testCasesQueue_.Reverse());
        }
    }
}
