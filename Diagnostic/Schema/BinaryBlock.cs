﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Diagnostic.Model.Operations;

namespace Diagnostic.Schema
{
    public class BinaryBlock : IBlock
    {
        private readonly BinaryOperation resultFunction_;
        private readonly IBlock[] inputs_;

        public string BlockId { get; }

        public BinaryBlock(
            string id,
            BinaryOperation resultFunction,
            params IBlock[] inputs)
        {
            BlockId = id;
            resultFunction_ = resultFunction;
            inputs_ = inputs;
        }

        public IEnumerable<IBlock> GetDependencies()
        {
            return inputs_;
        }

        public TruthTable GetTruthTable()
        {
            var rows = new List<TruthTableRow>();

            var combinations = inputs_
                .Select(i => new LogicalValue[] { LogicalValue.Set, LogicalValue.NotSet })
                .ToArray();

            var temp = new LogicalValue[inputs_.Length];
            void Populate(int index)
            {
                if (index >= combinations.Length)
                {
                    var result = Calculate(temp);
                    var row = new TruthTableRow(temp.ToArray(), result);
                    rows.Add(row);
                    return;
                }
                foreach (var currentValue in combinations[index])
                {
                    temp[index] = currentValue;
                    Populate(index + 1);
                }
            }
            Populate(0);
            return new TruthTable(inputs_, rows);
        }

        public override string ToString()
        {
            return BlockId;
        }

        public LogicalValue Calculate(LogicalValue[] input)
        {
            return resultFunction_(input);
        }
    }
}
