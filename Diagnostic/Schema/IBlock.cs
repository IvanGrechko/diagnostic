﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Schema
{
    public interface IBlock
    {
        string BlockId { get; }

        IEnumerable<IBlock> GetDependencies();

        TruthTable GetTruthTable();

        LogicalValue Calculate(LogicalValue[] input);
    }

    public interface IInput : IBlock
    {

    }
}
