﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Schema
{
    public interface ISchema
    {
        string[] GetAllBlockIds();
        IBlock[] GetAllBlocks();
        IBlock GetBlock(string block);
        IBlock GetOutputBlock();
        IBlock TryGetParent(string blockId);

        IInput[] GetInputBlocks();
    }
}
