﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Schema
{
    public interface ISchemaFactory
    {
        ISchema CreateSchema();
    }
}
