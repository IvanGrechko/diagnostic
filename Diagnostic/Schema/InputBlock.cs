﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Schema
{
    public class InputBlock : IInput
    {
        public string BlockId { get; }

        public InputBlock(string id)
        {
            BlockId = id;
        }

        public IEnumerable<IBlock> GetDependencies()
        {
            return Enumerable.Empty<IBlock>();
        }

        public override string ToString()
        {
            return BlockId;
        }

        public TruthTable GetTruthTable()
        {
            var rows = new List<TruthTableRow>()
            {
                new TruthTableRow(new [] {LogicalValue.Set}, LogicalValue.Set),
                new TruthTableRow(new [] {LogicalValue.NotSet}, LogicalValue.NotSet),
            };
            return new TruthTable(new[] { this }, rows);
        }

        public LogicalValue Calculate(LogicalValue[] input)
        {
            if (input.Length != 1) throw new ArgumentException();
            return input[0];
        }
    }
}
