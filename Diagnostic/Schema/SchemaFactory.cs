﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Schema
{
    public class SchemaFactory : ISchemaFactory
    {
        public ISchema CreateSchema()
        {
            var x1 = ProcessBlock(new InputBlock("x1"));
            var x2 = ProcessBlock(new InputBlock("x2"));
            var x3 = ProcessBlock(new InputBlock("x3"));
            var x4 = ProcessBlock(new InputBlock("x4"));
            var x5 = ProcessBlock(new InputBlock("x5"));
            var x6 = ProcessBlock(new InputBlock("x6"));
            var x7 = ProcessBlock(new InputBlock("x7"));

            var f1 = ProcessBlock(new BinaryBlock("f1", Model.Operations.AndNot, x1, x2));
            var f2 = ProcessBlock(new UnaryBlock("f2", x3, Model.Operations.Not));
            var f3 = ProcessBlock(new BinaryBlock("f3", Model.Operations.OrNot, x5, x6));
            var f4 = ProcessBlock(new BinaryBlock("f4", Model.Operations.AndNot, x4, f3, x7));
            var f5 = ProcessBlock(new BinaryBlock("f5", Model.Operations.Xor, f2, f4));
            var f6 = ProcessBlock(new BinaryBlock("f6", Model.Operations.And, f1, f5));

            return new SchemaProvider(f6);
        }

        protected virtual IBlock ProcessBlock(IBlock block)
        {
            return block;
        }
    }
}
