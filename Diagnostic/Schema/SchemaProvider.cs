﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diagnostic.Schema
{
    public class SchemaProvider : ISchema
    {
        private Dictionary<string, IBlock> parent_ = new Dictionary<string, IBlock>();
        private Dictionary<string, IBlock> instances = new Dictionary<string, IBlock>();
        public SchemaProvider(IBlock outputBlock)
        {
            Register(outputBlock);
        }

        public void Register(IBlock block)
        {
            var queue = new Queue<IBlock>();
            queue.Enqueue(block);

            while (queue.TryDequeue(out var current))
            {
                instances[current.BlockId] = current;
                foreach (var dependecy in current.GetDependencies())
                {
                    parent_[dependecy.BlockId] = current;
                    queue.Enqueue(dependecy);
                }
            }
        }

        public IBlock GetBlock(string block)
        {
            return instances[block];
        }

        public IBlock TryGetParent(string blockId)
        {
            if (parent_.TryGetValue(blockId, out var instance))
                return instance;
            return null;
        }

        public IBlock[] GetAllBlocks()
        {
            return instances.Values.ToArray();
        }

        public string[] GetAllBlockIds()
        {
            return instances.Keys.ToArray();
        }

        public IBlock GetOutputBlock()
        {
            return instances.Values.First(i => !parent_.ContainsKey(i.BlockId));
        }

        public IInput[] GetInputBlocks()
        {
            return instances.Values.OfType<IInput>().ToArray();
        }
    }
}
