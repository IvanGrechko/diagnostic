﻿using Diagnostic.Model;
using System;
using System.Collections.Generic;
using static Diagnostic.Model.Operations;

namespace Diagnostic.Schema
{
    public class UnaryBlock : IBlock
    {
        public string BlockId { get; }

        private IBlock input1_;
        private UnaryOperation resultFunction_;

        public UnaryBlock(string id, IBlock input1,
            UnaryOperation resultFunction)
        {
            BlockId = id;
            input1_ = input1;
            resultFunction_ = resultFunction;
        }

        public IEnumerable<IBlock> GetDependencies()
        {
            yield return input1_;
        }

        public TruthTable GetTruthTable()
        {
            var rows = new List<TruthTableRow>()
            {
                new TruthTableRow(new [] {LogicalValue.Set}, resultFunction_(LogicalValue.Set)),
                new TruthTableRow(new [] {LogicalValue.NotSet}, resultFunction_(LogicalValue.NotSet)),
            };
            return new TruthTable(new[] { input1_ }, rows);
        }

        public override string ToString()
        {
            return BlockId;
        }

        public LogicalValue Calculate(LogicalValue[] input)
        {
            if (input.Length != 1) throw new ArgumentException();
            return resultFunction_(input[0]);
        }
    }
}
