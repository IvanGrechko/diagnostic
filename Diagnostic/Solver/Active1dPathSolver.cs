﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostic.Model;
using Diagnostic.Schema;

namespace Diagnostic.Solver
{
    public class Active1dPathSolver : ISolver
    {
        private readonly ISchema solver_;

        public Active1dPathSolver(ISchema solver)
        {
            solver_ = solver;
        }
        public TestCasesTable Solve(IBlock errorBlock, LogicalValue errorConstValue)
        {
            var errorResult = SolveForChild(errorBlock, solver_, !errorConstValue);
            var passedErrorResult = SolveForParent(errorBlock, solver_, errorConstValue);

            errorResult.Merge(passedErrorResult);

            return errorResult.GetTestCasesTable(solver_);
        }

        private static SolverResult SolveForParent(IBlock block, ISchema solver, LogicalValue passedValue)
        {
            var parent = solver.TryGetParent(block.BlockId);
            var result = new SolverResult();
            if (parent == null) return result;

            var truthTable = parent.GetTruthTable();
            var currentBlockIndex = truthTable.Variables.FindIndex(v => v.BlockId == block.BlockId);

            var affectedRows = truthTable.Rows
                .Where(r =>
                {
                    if (r.InputValues[currentBlockIndex] != passedValue) return false;

                    var values = r.InputValues.ToArray();
                    var currentValue = parent.Calculate(values);
                    values[currentBlockIndex] = !passedValue;
                    var remainingValue = parent.Calculate(values);

                    return currentValue != remainingValue;
                    })
                .ToList();

            foreach (var affectedRow in affectedRows)
            {
                var rowResult = new SolverResult();
                for (int varIndex = 0; varIndex < affectedRow.InputValues.Count; varIndex++)
                {
                    if (varIndex != currentBlockIndex)
                    {
                        var childResult = SolveForChild(truthTable.Variables[varIndex], solver, affectedRow.InputValues[varIndex]);
                        rowResult.Merge(childResult);
                    }
                }

                result.MergeSameVariables(rowResult);
            }

            var parentResult = SolveForParent(parent, solver, passedValue);
            result.Merge(parentResult);

            return result;
        }
        private static SolverResult SolveForChild(IBlock block, ISchema solver, LogicalValue expectedValue)
        {
            var truthTable = block.GetTruthTable();
            var affectedRows = truthTable.Rows.Where(r => r.Result == expectedValue).ToList();

            var result = new SolverResult();
            foreach (var affectedRow in affectedRows)
            {
                var rowResult = new SolverResult();
                for (int variableIndex = 0; variableIndex < truthTable.Variables.Count; variableIndex++)
                {
                    var variable = truthTable.Variables[variableIndex];
                    var varExpectedValue = affectedRow.InputValues[variableIndex];

                    if (variable.BlockId == block.BlockId)
                    {
                        rowResult.Add(variable.BlockId, varExpectedValue);
                    }
                    else
                    {
                        rowResult.Merge(SolveForChild(variable, solver, varExpectedValue));
                    }
                }

                result.MergeSameVariables(rowResult);
            }

            return result;
        }
    }
}
