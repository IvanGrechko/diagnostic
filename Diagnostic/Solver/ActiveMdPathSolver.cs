﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostic.Model;
using Diagnostic.Schema;

namespace Diagnostic.Solver
{
    public class ActiveMdPathSolver : ISolver
    {
        private readonly ISchema dependenciesSolver_;

        public ActiveMdPathSolver(ISchema dependenciesSolver)
        {
            dependenciesSolver_ = dependenciesSolver;
        }
        public TestCasesTable Solve(IBlock errorBlock, LogicalValue errorConstValue)
        {
            var cubeTable = CreateSingularityCube(errorBlock);
            var primitiveDTable = CreatePrimitiveDCube(cubeTable, errorConstValue);
            var allVariables = dependenciesSolver_.GetAllBlocks();

            var initial = CreateInitial(allVariables);
            var result = IntersectD(initial, primitiveDTable);

            var processedBlocks = new HashSet<string>() { errorBlock.BlockId };
            var parent = dependenciesSolver_.TryGetParent(errorBlock.BlockId);
            while (parent != null)
            {
                var parentSCube = CreateSingularityCube(parent);
                var parentDCube = CreateDCube(parentSCube);
                result = IntersectD(result, parentDCube);

                processedBlocks.Add(parent.BlockId);
                parent = dependenciesSolver_.TryGetParent(parent.BlockId);
                
            }

            var remainingBlocks = dependenciesSolver_.GetAllBlockIds()
                .Where(id => !processedBlocks.Contains(id))
                .Select(id => dependenciesSolver_.GetBlock(id))
                .Where(b => !(b is IInput))
                .ToList();

            foreach (var remainingBlock in remainingBlocks)
            {
                var remainingSCube = CreateSingularityCube(remainingBlock);
                result = IntersectD(result, remainingSCube);
            }

            return ToTestCasesTable(result);
        }

        private CubeTable CreateSingularityCube(IBlock block)
        {
            var truthTable = block.GetTruthTable();
            var varCount = truthTable.Variables.Count;
            var cubes = new List<CubeTableRow>();

            foreach (var outputGroup in truthTable.Rows.GroupBy(r => r.Result))
            {
                var affectedRows = outputGroup.Select((r,i) => new { Row = r, Index = i }).ToList();
                var processedIndecies = new HashSet<int>();
                for (int index = 0; index < varCount; index++)
                {
                    var groupedByIndexedVariable = affectedRows.GroupBy(ar => ar.Row.InputValues[index]);
                    foreach (var group in groupedByIndexedVariable)
                    {
                        if (group.Count() == 2 * (varCount - 1))
                        {
                            //can be replaced with x
                            var cubeValues = Enumerable.Range(0, varCount)
                                .Select(i => i == index ? (CubeValue)group.Key : CubeValue.X)
                                .ToList();
                            var cubeRow = new CubeTableRow(cubeValues, outputGroup.Key);
                            cubes.Add(cubeRow);

                            foreach (var item in group)
                            {
                                processedIndecies.Add(item.Index);
                            }
                        }
                    }
                }

                var unProcessedRows = affectedRows.Where(r => !processedIndecies.Contains(r.Index)).ToList();
                foreach (var unProcessedRow in unProcessedRows)
                {
                    var cubeValues = unProcessedRow.Row.InputValues.Select(l => (CubeValue)l).ToList();
                    var cubeRow = new CubeTableRow(cubeValues, (CubeValue)unProcessedRow.Row.Result);
                    cubes.Add(cubeRow);
                }
            }

            return new CubeTable(truthTable.Variables.Append(block), cubes);
        }

        private CubeTable CreateDCube(CubeTable singularityCube)
        {
            var groups = singularityCube.Rows.GroupBy(r => r.Result).ToDictionary(r => r.Key, r => r.ToList());

            if (!groups.TryGetValue(CubeValue.Set, out var setgroup))
            {
                throw new Exception();//TODO
            }
            if (!groups.TryGetValue(CubeValue.NotSet, out var notSetGroup))
            {
                throw new Exception();//TODO
            }

            var resultDCubes = new List<CubeTableRow>();
            for (int setIndex = 0; setIndex < setgroup.Count; setIndex++)
                for (int notSetIndex = 0; notSetIndex < notSetGroup.Count; notSetIndex++)
                {
                    var setCube = setgroup[setIndex];
                    var notSetCube = notSetGroup[notSetIndex];

                    var directValues = new CubeValue[setCube.InputValues.Count];
                    for (int index = 0; index < directValues.Length; index++)
                    {
                        directValues[index] = CubeValue.Intersect(setCube.InputValues[index], notSetCube.InputValues[index]);
                    }

                    var reverseValues = new CubeValue[setCube.InputValues.Count];
                    for (int index = 0; index < directValues.Length; index++)
                    {
                        reverseValues[index] = CubeValue.Intersect(notSetCube.InputValues[index], setCube.InputValues[index]);
                    }

                    resultDCubes.Add(new CubeTableRow(directValues));
                    resultDCubes.Add(new CubeTableRow(reverseValues));
                }

            return new CubeTable(singularityCube.Variables, resultDCubes);
        }

        private CubeTable CreatePrimitiveDCube(CubeTable singularityCube, LogicalValue errorValue)
        {
            var reverseValue = !errorValue;
            var affectedRows = singularityCube.Rows.Where(r => r.Result == reverseValue);

            var errorCubeValue = errorValue.IsSet ? CubeValue.NotD : CubeValue.D;

            var dRows = affectedRows.Select(r =>
            {
                var values = r.InputValues.ToList();
                values[values.Count - 1] = errorCubeValue;
                return new CubeTableRow(values);
            })
                .ToList();
            return new CubeTable(singularityCube.Variables, dRows);
        }

        private CubeTable CreateInitial(IBlock[] variables)
        {
            var row = new CubeTableRow(variables.Select(v => CubeValue.X));
            return new CubeTable(variables, new[] { row });
        }

        private CubeTable IntersectD(CubeTable cubesLeft, CubeTable cubesRight)
        {
            var varLeftIndecies = cubesLeft.Variables.Select((v, i) => new { Index = i, Id = v.BlockId })
                .ToDictionary(r => r.Id, r => r.Index);
            var varRightIndecies = cubesRight.Variables.Select((v, i) => new { Index = i, Id = v.BlockId })
                .ToDictionary(r => r.Id, r => r.Index);

            var targetRows = new List<CubeTableRow>();
            var allKeys = varLeftIndecies.Keys.Union(varRightIndecies.Keys).Distinct().ToArray();
            foreach (var leftRow in cubesLeft.Rows)
                foreach (var rightRow in cubesRight.Rows)
                {
                    var resultValues = new CubeValue[allKeys.Length];
                    bool isAborted = false;
                    for (int resultKeyIndex = 0; resultKeyIndex < allKeys.Length; resultKeyIndex++)
                    {
                        string key = allKeys[resultKeyIndex];
                        var leftValue = CubeValue.X;
                        var rightValue = CubeValue.X;
                        if (varLeftIndecies.TryGetValue(key, out var leftIndex))
                            leftValue = leftRow.InputValues[leftIndex];
                        if (varRightIndecies.TryGetValue(key, out var rightIndex))
                            rightValue = rightRow.InputValues[rightIndex];

                        CubeValue result = null;
                        if (leftValue == rightValue || rightValue == CubeValue.X)
                        {
                            result = leftValue;
                        }
                        else if (leftValue == CubeValue.X)
                        {
                            result = rightValue;
                        }
                        else
                        {
                            //empty
                            isAborted = true;
                            break;
                        }

                        if (result != null)
                        {
                            resultValues[resultKeyIndex] = result;
                        }
                    }

                    if (!isAborted)
                    {
                        targetRows.Add(new CubeTableRow(resultValues));
                    }
                }

            var variables = allKeys.Select(key => dependenciesSolver_.GetBlock(key));
            return new CubeTable(variables, targetRows);
        }

        private TestCasesTable ToTestCasesTable(CubeTable table)
        {
            var variableIndecies = table.Variables.Select((v, i) => new { Index = i, Variable = v })
                .ToDictionary(p => p.Variable.BlockId);

            var inputVariables = table.Variables.OfType<IInput>().OrderBy(v => v.BlockId).ToList();
            var requiredIdecies = inputVariables.Select(v => variableIndecies[v.BlockId].Index).ToList();
            var testCasesRows = new List<TestCasesTableRow>();
            foreach (var cube in table.Rows)
            {
                foreach (var testCase in GetTestCaseRows(requiredIdecies, cube))
                {
                    testCasesRows.Add(new TestCasesTableRow(testCase));
                }
            }

            return new TestCasesTable(inputVariables, testCasesRows);
        }

        private IEnumerable<IList<LogicalValue>> GetTestCaseRows(IList<int> requiredIndecies, CubeTableRow sourceRow)
        {
            var results = new List<IList<LogicalValue>>();
            var buffer = new LogicalValue[requiredIndecies.Count];

            void Populate(int index)
            {
                if (index >= requiredIndecies.Count)
                {
                    results.Add(buffer.ToList());
                    return;
                }
                var targetIndex = requiredIndecies[index];
                var currentCubeValue = sourceRow.InputValues[targetIndex];
                if (currentCubeValue == CubeValue.Set || currentCubeValue == CubeValue.NotSet)
                {
                    buffer[index] = currentCubeValue == CubeValue.Set ? LogicalValue.Set : LogicalValue.NotSet;
                    Populate(index + 1);
                }
                else if (currentCubeValue == CubeValue.X)
                {
                    buffer[index] = LogicalValue.Set;
                    Populate(index + 1);
                    buffer[index] = LogicalValue.NotSet;
                    Populate(index + 1);
                }
                else
                {
                    throw new NotSupportedException();
                }
            }

            Populate(0);
            return results;
        }
    }
}
