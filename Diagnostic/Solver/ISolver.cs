﻿using Diagnostic.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diagnostic.Solver
{
    public interface ISolver
    {
        Model.TestCasesTable Solve(IBlock errorBlock, Model.LogicalValue errorConstValue);
    }
}
