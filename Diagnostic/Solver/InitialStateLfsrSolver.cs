﻿using Diagnostic.Model;
using Diagnostic.Random;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Diagnostic.Solver
{
    public class InitialStateLfsrSolver
    {
        public (TestCasesTableRow, int) FindInitialState(IList<TestCasesTableRow[]> testCases, int variablesCount)
        {
            var minTickCount = 10000;
            TestCasesTableRow stateRow = null;

            var states = byte.MaxValue;
            for (byte state = 1; state < states; state++)
            {
                var samplesGenerator = new LInearfeedbackShiftRegister(state);
                var initialStateRow = samplesGenerator.CurrentState.ToList();
                var testCasesGenerator = new TestCasesGenerator(samplesGenerator, variablesCount);

                var testCasesHashSet = testCases
                    .Select(t => new HashSet<TestCasesTableRow>(t, TestCasesTableRow.DefaultEqualityComaprer))
                    .ToList();

                var evaluationCount = 0;
                foreach (var testCase in testCasesGenerator.GenerateTestCases())
                {
                    testCasesHashSet.RemoveAll(row => row.Contains(testCase));
                    //if (testCasesHashSet.Remove(testCase))
                    //{
                        if (testCasesHashSet.Count == 0) break;
                    //}

                    if (evaluationCount > 10000 || evaluationCount > minTickCount) break;

                    evaluationCount++;
                }

                if (minTickCount > evaluationCount)
                {
                    stateRow = new TestCasesTableRow(initialStateRow);
                    minTickCount = evaluationCount;
                }
            }

            return (stateRow, minTickCount);
        }
    }
}
